import math
import matplotlib.pyplot as plt


def function(x):
    return math.exp(x)*math.cos(x)


def derivative(x):
    return math.exp(x)*math.cos(x) - math.exp(x)*math.sin(x)


def num_derivative(x, alpha):
    return (function(x+alpha)-function(x))/alpha


def delta(x, alpha):
    return math.fabs(derivative(x) - num_derivative(x, alpha))


if __name__ == "__main__":
    x = 2
    deltas = []
    log_deltas = []
    alphas = [10**(-i) for i in range(20)]
    log_alphas = [math.log10(10**(-i)) for i in range(20)]
    for alpha in alphas:
        deltas.append(delta(x, alpha))
        log_deltas.append(math.log10(delta(x, alpha)))

    plt.plot(log_alphas, log_deltas)
    plt.xticks([-i*2 for i in range(10)])
    plt.yticks([-i for i in range(10)])
    plt.xlabel('log 10 alpha')
    plt.ylabel('log 10 delta')
    plt.savefig('graph.jpg')